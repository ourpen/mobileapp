#MobileApp
##Contribution

###Make a fork and clone to your local

	git clone https://<your_name>@bitbucket.org/<your_name>/mobileApp.git
	
Follow this link to setup your own repo: https://help.github.com/articles/fork-a-repo/

###Run this to install all the dependencies:

	npm install

**Reminder**: do not do -g to install the package globally

I do not know how to have a virtual environment for development currently, so
you can add ./node_modules/.bin to you shell rc file. I use zsh, this is what add to my ~/.zshrc:

	export PATH=~/Repos/mobileApp/node_modules/.bin:$PATH

###Build and test on IOS

First, you need to have Xcode installed.

To build the IOS app locally for the first time, you need to change recursively change the ownership of all the files. Go to your app home directory, run (to resolve [this issue](https://github.com/driftyco/ionic/issues/4052)):

	sudo chown -R *
	ionic build ios
	ionic emulate ios

###Build and test on Andriod

Testing on Android is much easier and faster. To test on the device, simply plug it in, and run

        ionic run android

If this doesn’t work, make sure you have USB debugging enabled on your device, as described on the Android developer site.


	
###Test in browser

	ionic serve
###Unit test

To ensure continous development, we need to have more than 90% unit test coverage for all the javascript files that we writes.

####Framework

We are using Jasmine as unit test framework. For more information about how to write unit tests, please refer to their [official documentation](http://jasmine.github.io/edge/introduction.html).

####File locations

Our unit tests are located in:

	/test/unit/<the_file_name_you_test>_test.js
	
To be more clear, the test file should have the same directory with the file you are testing, with "_test" as suffix.

####How to run unit tests

You can simply run:

	npm test
	
This will show you if your tests are passed, and how much coverage you have.

###Rule of merging pull request

1. You have more than 90% unit test coverage after your development.
2. All unit tests are passed.
3. If you are making new file, one of the other team member should give you a "ship it" on your pull request.
4. If you are changing an exsiting file, one of the file owner must give you a "ship it" on your pull request.